<?php
	$gender = array(0 => 'Nam',1 => 'Nữ');
	$faculty = array('Khoa học máy tính' => 'MAT','Khoa học vật liệu' => 'KDL');
?>
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
	<link rel='stylesheet' type='text/css'  href='style.css'>
</head>

<body>

	<div>
	
		<form>

			<div>
			
				<div id='input'>
					<label id='label'>Họ và tên</label>
					<input type='text' name='username'/>
				</div>
				
				<div id='input'>
					<label id='label'>Giới tính</label>
					<?php
						for($i=0;$i<=1;$i++){
							echo"
								<input type='radio' name='gender'> <label id='gender'>".$gender[$i]."</label>";
						}
					?>	
				</div> 
				
				<div id='input'>
					<label id='label'>Phân khoa</label>
					<select name='faculty'>
						<option>  </option>
						<?php
							foreach($faculty as $item => $value){
								echo "<option>".$item."</option>";
							}
						?>
					</select>
				</div>
				
			</div>

			<div>
				<button>Đăng ký</button>
			</div>

		</form>
		
	</div>
	
</body>